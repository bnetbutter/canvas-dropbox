#TODO setup script
# install 'dropbox serve' as startup service

from setuptools import setup, find_packages

setup(
    name="canvas-dropbox",
    description="A simple drop box service for canvas assignments",
    url="https://gitlab.com/bnetbutter/canvas-dropbox",
    version="0.1",
    author="Kevin Lai",
    author_email="zlnh4@umsystem.edu",
    license="GPL v3",
    packages=find_packages(),
    scripts=["dropbox/dropbox.py"],
    python_requires=">= 3.8",
    install_requires = [
        "canvasapi",
        "pillow",
    ],
)