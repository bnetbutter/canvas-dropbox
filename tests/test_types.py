import unittest
import sys
import os

from dropbox._struct import Struct, Schema

class Test_Struct(unittest.TestCase):

    def test_assignable(self):    
        class Point(Struct):
            x: int
        
        p = Point()
        p.x = 10
    
    def test_bad_type_assignment(self):
        class Point(Struct):
            x: int
        
        p = Point()
        with self.assertRaises(TypeError):
            p.x = "?"

    def test_nonmember_assignment(self):
        class Point(Struct):
            ...
        
        p = Point()
        with self.assertRaises(AttributeError):
            p.x = 10
    
    def test_castable(self):
        class Point(Struct):
            x: str
            y: int 
       
        p = Point()
        p.x = 10
        p.y = "15"

        self.assertTrue(isinstance(p.x, str))
        self.assertTrue(isinstance(p.y, int))
    

    def test_subclassing_type(self):
        class Point(Struct):
            x: int
            y: int
        
        class Circle(Point):
            rad: int
        
        c = Circle()
        c.x = 10
        c.y = 20
        c.rad = 30
    
    
    def test_subclass_type_overload(self):
        class Point(Struct):
            x: int
            y: int

        class Circle(Point):
            x: float
            rad: int
        
        c = Circle()
        c.x = 1
        c.y = 10
        c.rad = 10        
        self.assertTrue(isinstance(c.x, float))
        self.assertTrue(isinstance(c.y, int))
        self.assertTrue(isinstance(c.rad, int))

        p = Point()
        p.x = 10
        p.y = 10
        self.assertTrue(isinstance(p.x, int))
        self.assertTrue(isinstance(p.y, int))
    
    def test_setattr_defined(self):
        class Point(Struct):
            x: int

            def __setattr__(self, name, value):
                super().__setattr__(name, value/2)
        
        p = Point()
        p.x = 20
        self.assertEqual(p.x, 10)

    def test_nodict(self):
        class Point(Struct): 
            ...
        
        p = Point()

        with self.assertRaises(Exception):
            p.__dict__ = 0



class TestSchema(unittest.TestCase):


    def test_shema_definition(self):
        class TestSchema(Schema):
            number: int
            path: str
          
        dct = TestSchema()
        dct.number = 10
        dct.path = "/path/to/something"
        
        self.assertEqual(dct.number, 10)
        self.assertEqual(dct.path, "/path/to/something")

        dct["number"] = 11
        dct["path"] = "/path/to/another"

        self.assertEqual(dct["number"], 11)
        self.assertEqual(dct["path"], "/path/to/another")


    def test_attribute_error(self):
        class TestSchema(Schema):
            pass
            
        dct = TestSchema()
        
        with self.assertRaises(AttributeError):
            dct.number = 10

        with self.assertRaises(AttributeError):
            dct.number
        
        with self.assertRaises(AttributeError):
            dct["number"] = 10
        
        with self.assertRaises(AttributeError):
            dct["number"]
    

    def test_cast(self):
        class TestSchema(Schema):
            name: str
            id: int

        cast = TestSchema({"name":10, "id": 10})
  
        self.assertTrue(isinstance(cast.name, str))
        self.assertTrue(isinstance(cast.id, int))
    

    def test_json_serialize(self):
        import json
        class TestSchema(Schema):
            name: str
            id: int
        
        test_obj = TestSchema({"name": 10, "id": 10})
        ref_obj = {"name": "10", "id": 10}
        self.assertEqual(json.dumps(test_obj), json.dumps(ref_obj))
    
    
    def test_invalid_classdef(self):
        with self.assertRaises(AttributeError):
            class TestSchema(Schema):
                def __getitem__(self):
                    pass
    

    def test_allow_missing_name(self):
        class TestSchema(Schema):
            name: str
        obj = TestSchema()
        self.assertEqual(obj.name, None)
        

        
if __name__ == "__main__":
    unittest.main()