class _StructType(type):
    
    def __new__(cls, name, bases, namespace):
        slots = set()
        for base in bases:
            for k in getattr(base, "_fields_", {}):
                slots.add(k)
        for k in namespace.get("__annotations__", {}):
            if k not in namespace:
                slots.add(k)

        namespace["__slots__"] = slots
        return super().__new__(cls, name, bases, namespace)


    def __init__(self, name, bases, namespace):
        self._fields_ = {}
        for base in reversed(self.__mro__):
            self._fields_.update(getattr(base, "__annotations__", {}))


class Struct(metaclass=_StructType):
    def __setattr__(self, name, value):
        fields = type(self)._fields_
        if name in fields and isinstance(value, fields[name]):
            super().__setattr__(name, value)
        else:
            try:
                value = fields[name](value)
            except KeyError:
                raise AttributeError(name)           
            except ValueError:
                raise TypeError(
                    f"Cannot cast {type(value)} to {fields[name]}"
                )
        super().__setattr__(name, value)

            

class _SchemaType(_StructType):
    
    def __init__(self, name, bases, namespace):
        super().__init__(name, bases, namespace)
        restricted_names = [
            "__getattr__",
            "__setattr__",
            "__setitem__",
            "__getitem__",
            "__init__"
        ]

        if any(name in namespace for name in restricted_names):
            raise AttributeError(
                f"Class definition contains one or more of {restricted_names}"
            )

        def __getattr__(obj, name):
            if name not in self._fields_:
                raise AttributeError()
            if name not in obj:
                return None
            return dict.__getitem__(obj, name)

        def __setattr__(obj, name, value):
            if name not in self._fields_:
                raise AttributeError()
            if isinstance(value, self._fields_[name]):
                return dict.__setitem__(obj, name, value)
            try:
                value = self._fields_[name](value)
            except ValueError:
                raise TypeError(
                    f"Cannot cast {type(value)} to {self._fields_[name]}"
                )
            else:
                return dict.__setitem__(obj, name, value)

        def __init__(obj, *args, **kwargs):
            dict.__init__(obj, *args, **kwargs)
            for k, v in zip(obj.keys(), obj.values()):
                obj[k] = v # cast values

        self.__getattr__ = __getattr__
        self.__setattr__ = __setattr__
        self.__getitem__ = __getattr__
        self.__setitem__ = __setattr__
        self.__init__ = __init__
    

class Schema(dict, metaclass=_SchemaType):
    """ Base class for JSON schema """


__all__ = ["Struct", "Schema"]