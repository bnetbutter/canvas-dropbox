"""JSON type definitions"""

from ._struct import Schema

class Dropbox(Schema):
    url: str        # canvas provider
    token: str      # access token
    path: str       # dropbox root
    courses: list   # list of courses
    mtime: int      # posix time of last pull


class Course(Schema):
    name: str           # pretty print name of course
    assignments: list   # list of assignments
    course_id: int      


class Assignment(Schema):
    name: str           # pretty print name of assignment
    assignment_id: int  
    course_id: int
    due_at: int         # posix utc timestamp of assignment due date/time


class Submission(Schema):
    mtime: int          # posix time of last edit
    status: int         # enumerated file status. ie sucess, fail, pending
    assignment_id: int  # id of assignment this file was uploaded to
    upload_format: str  # file type of submission after file conversion


class CourseFolder(Schema):
    exclude: str        # re pattern of files excluded from upload
    pre_upload: str     # path to shell script called before file is uploaded
    post_upload:str     # path to shell script called after file is uploaded
    on_success: str     # path to shell script called on successful upload
    on_failure: str     # path to shell script called on failed upload
    submissions: list   # list of submission objects

