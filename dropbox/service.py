import tkinter as tk
from tkinter import ttk
import asyncio
import threading
import datetime
import time
import sys
import enum
import os
import logging
import queue
import pathlib
from canvasapi import Canvas
from PIL import Image
from contextlib import contextmanager

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(message)s")
handler = logging.StreamHandler()
handler.setFormatter(formatter)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

MTIME_FILE = "mtime.json"
PENDING =   0
CANCELED =  1
SUBMITTED = 2
MODIFIED =  3
FAILED =    4
SUCCESS =   5

try:
    from .dropbox import *
except:
    from dropbox import *



class Pointer:
    _value_ = None

    @property
    def value(self):
        return self._value_
    
    @value.setter
    def value(self, value):
         self._value_ = value
        
tokenptr = Pointer()

def serve():
    token_ptr = Pointer()
    token_ptr.value = dropbox_pull()
    threading.Thread(
        target=_thread_pull_worker,
        args=(token_ptr)
    )
    asyncio.run(_main(token_ptr))


async def _main(
    token_ptr: Pointer
) -> None:

    courses = token_ptr.value["courses"]
    rootdir = token_ptr.value[PATH]
    coroutines = []
    
    managed_folders = (
        (course_id, os.path.join(rootdir, course_info["name"]))
        for course_id, course_info in zip(courses.keys(), courses.values())
            if course_info["assignments"]
    )

    for course_id, folder in managed_folders:
        # Windows does not allow the ":" excecpt as 'drive separator' 
        # char. ie.
        #   `C:\drive`  is ok 
        #   `C:\drive:` is not ok
        if os.name == "nt":
            folder = folder[2:].replace(":", "")

        if not os.path.exists(folder):
            os.mkdir(folder)
            # TODO track file mtimes. resubmit on save
            with open(os.path.join(folder, MTIME_FILE), "w") as fp:
                json.dump({}, fp)
        coroutines.append(_async_folder_worker(
                canvas_create(),
                course_id,
                folder,
                token_ptr,
            ))
    
    await asyncio.gather(*coroutines)
  


def get_managed_files(folderpath):
    for filename in os.listdir(folderpath):
        if filename != MTIME_FILE:
            filepath = os.path.join(folderpath, filename)
            if os.path.isfile(filepath):
                yield filepath
@contextmanager
def folderdata(filepath):
    try:
        with open(filepath, "r") as fp:
            dct = json.load(fp)
        yield dct
    finally:
        with open(filepath, "w") as fp:
            json.dump(dct, fp, indent=4)
    

async def _async_folder_worker(
    canvas: Canvas,
    course_id: int,
    folderpath: str,
    token_ptr: Pointer,
) -> None:
    while True:
        mtime_path = os.path.join(folderpath, MTIME_FILE)
        with open(mtime_path, "r") as fp:
            mtime_record = json.load(fp)
        
        # New file was added to folder
        new_submissions = []
        for ent in get_managed_files(folderpath):
            if ent not in mtime_record:
                new_submissions.append(os.path.join(ent))
                logging.info(f"received file '{ent}'")
        
        if new_submissions:
            for new_sub in new_submissions:
                mtime_record[new_sub] = {
                    "time": int(os.path.getmtime(new_sub)),
                    "status": PENDING,
                    "id": -1,
                }
                # Run UI thread
                threading.Thread(
                    target=_thread_ui_worker,
                    args=(
                        canvas,
                        course_id,
                        new_sub,
                        token_ptr
                    ),
                ).start()

            with open(mtime_path, "w") as fp:
                json.dump(mtime_record, fp, indent=4)
        
        await asyncio.sleep(1/30)


def upload_assignment(
        canvas, course_id, assignment_id, filepath
) -> int:
    
    try:

        assn = canvas.get_course(
            course_id
        ).get_assignment(
            assignment_id
        )

        path, ext = os.path.splitext(filepath)
        if ext == ".png":
            # need to convert .png to pdf for math class

            base = os.path.basename(path)
            img = Image.open(filepath)

            if not os.path.exists(tmp := os.path.join(
                os.path.dirname(filepath), "tmp"
            )):
                os.mkdir(tmp)
                logger.debug(f"creating tmp directory at {tmp}")
            
            tmp = os.path.join(tmp, base + ".pdf")
            img = img.convert("RGB")
            img.save(tmp)
            logger.debug(f"pdf saved to {tmp}")
            logger.info(f"Uploading {tmp} ...")
            assert os.path.exists(tmp)

            r = assn.submit({"submission_type": "online_upload"}, tmp)
            logger.debug(f"submit returned {r} of type {type(r)}")
            os.remove(tmp)
        else:
            logger.info(f"Uploading {filepath} ...")
            r = assn.submit({"submission_type":"online_upload"}, filepath)
            logger.debug(f"submit returned {r}")
        return 0
    except:
        exepttype, _, _ = sys.exc_info()
        logger.error(f"Unhandled exception {exepttype}")


def _thread_pull_worker(
        token_ptr: Pointer,
        interval: int = None,
) -> None:
    # pull once a day
    interval = 86400 if interval is None else interval
    while True:
        time.sleep(interval)
        tokenptr.value = dropbox_pull(check_mtime=False)
    

lock = threading.Lock()

def _thread_ui_worker(
        canvas: Canvas,
        course_id: int,
        filepath: int,
        tokenptr: Pointer,

) -> None:
    
    courses = tokenptr.value["courses"]
    assert course_id in courses

    timestamp = int(datetime.now().timestamp())
    course_assignments = courses[course_id]["assignments"]
    prev_assignments = [
        assignment for assignment in course_assignments
            if timestamp >= assignment["due_at"]
    ]
    next_assignments = [
        assignment for assignment in course_assignments
            if timestamp < assignment["due_at"]
    ]

    with lock: # one tcl instance per process.
        tk._default_root = None
        master = tk.Tk()
        master.attributes("-topmost", True)
        master.title("canvas-dropbox")
        label = tk.Label(
            master=master,
            text=os.path.basename(filepath),
            font=("Courier", 20),
            width=50,
        )
        lstbox = AssignmentList(
            master=master,
            assignments=next_assignments,
            width=50,
            font=("Courier", 16)
        )
        prevbox = AssignmentList(
            master=master,
            assignments=prev_assignments,
            width=50,
            font=("Courier", 16)
        )
        
        logger_output = TextHandler(
            master=master,
            width=50,
            height=6,
        )
        logger_output.setLevel(logging.DEBUG)
        logger_output.setFormatter(formatter)
        progressbar = ttk.Progressbar(
            master=master,
            mode="indeterminate",
            orient=tk.HORIZONTAL,
        )

        upload_args = lambda: [canvas, course_id, lstbox.get(), filepath]
        
        def on_submit(): 
            thread = threading.Thread(
                target=upload_assignment,
                args=upload_args()
            )
            thread.start()
            progressbar.start(10)
            while thread.is_alive():
                master.update()
            time.sleep(2)
            master.destroy()

        def on_delete():
            master.destroy()

        
        submit = tk.Button(
            master=master,
            text="Submit",
            font=("Courier", 20),
            command=on_submit,
        )
        delete = tk.Button(
            master=master,
            text="Delete",
            font=("Courier", 20),
            command=on_delete,
        )
        swap = tk.Button(
            master=master,
            font=("Courier", 16),
            text="Due At"
        )
        def on_swap():
            nonlocal upload_args

            if swap["text"] == "Past Due":
                logger.info("Viewing future assignments.")
                swap["text"] = "Due At"
                upload_args = lambda: [
                    canvas, course_id, lstbox.get(), filepath
                ]
                lstbox.lift()

            elif swap["text"] == "Due At":
                logger.info("Viewing past assignments.")
                swap["text"] = "Past Due"
                upload_args = lambda: [
                    canvas, course_id, prevbox.get(), filepath
                ]
                prevbox.lift()
        swap["command"] = on_swap


        label.grid(row=0, column=0, columnspan=2, sticky="nswe")
        swap.grid(row=1, column=1, sticky="nswe")
        lstbox.grid(row=2, column=0, columnspan=2, sticky="nswe")
        prevbox.grid(row=2, column=0, columnspan=2, sticky="nswe")
        progressbar.grid(row=3, column=0, columnspan=2, sticky="nswe")
        
        logger_output.grid(row=4, column=0, columnspan=2, sticky="nswe")
        submit.grid(row=5, column=0, sticky="nswe", padx=10, pady=10)
        delete.grid(row=5, column=1, sticky="nswe", padx=10, pady=10)

        lstbox.lift()
        try:
            logger.addHandler(logger_output)
            master.mainloop()
        finally:
            for handler in list(logger.handlers):
                if handler == logger_output:
                    return logger.handlers.remove(handler)
            assert 0

class AssignmentList(tk.Listbox):

    def __init__(self, *args, assignments=[], **kwargs):
        super().__init__(*args, **kwargs)
        self.selections = []
        for assignment in assignments:
            due_at = datetime.fromtimestamp(
                assignment["due_at"]
            ).strftime("%m/%d %I:%M %p")
            self.insert(
                tk.END, 
                f"{due_at} {assignment['name']}"
            )
            self.selections.append(assignment["id"])
        self.activate(0)
    
    def get(self):
        return self.selections[self.index(tk.ACTIVE)]
    

class FileExtensionList(tk.Listbox):

    def __init__(self, *args, supported=[], **kwargs):
        super().__init__(*args, **kwargs)
        for ext in supported: # supported conversions for this file format
            self.insert(
                tk.END,
                f"{ext}"
            )


class TextHandler(tk.Text, logging.StreamHandler):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        logging.StreamHandler.__init__(self)

    def emit(self, record):
        try:
            msg = self.format(record) + "\n"
            self.insert(tk.END, msg)
            self.see(tk.END)
        except RecursionError:
            raise
        except Exception:
            self.handleError(record)
    
    def __hash__(self):
        return threading.get_ident()

__all__ = ["serve"]