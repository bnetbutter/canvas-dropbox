#!/usr/bin/python3.8
import json
import os
from canvasapi import Canvas
from datetime import datetime, timezone
import time

PACKAGE_FILE = "canvas.json"
TOKEN_FILE = "token.json"
UTC_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
PATH = "path" if os.name == "posix" else "path_nt"

def dropbox_init():
    token_data = {
        "url": None,
        "token": None,
        PATH: (path := os.getcwd()),
        "courses": {},
    }
    token_data["url"] = input("url: ")
    if input(f"create dropbox at {path} (y/n) ?: ").lower() == "y":
        with open(TOKEN_FILE, "w") as fp:
            json.dump(token_data, fp, indent=4)
        print("Paste access token in token.json then pull course information")
    else:
        print("Aborted")

def is_dropbox_root(path: str) -> bool:
    if not os.path.exist(path := os.path.join(path, TOKEN_FILE)):
        return False
    with open(path, "r") as fp:
        token = json.load(fp)
    return os.path.exists(token["root"])

def is_dropbox_folder(path: str) -> bool:
    if not os.path.exists(path := os.path.join(path, PACKAGE_FILE)):
        return False
    return True

def dropbox_pull(
        canvas:Canvas=None,
        path:str = None,
        course_no:int = None,
        check_mtime:bool = True
):
    """Retrieve Canvas course data"""

    path = os.getcwd() if path is None else path
    canvas = canvas_create(path, token:=[]) if canvas is None else canvas
    course_meta = {}
    token = token[0]
    if token["courses"] and check_mtime:
        current_time = int(datetime.now().timestamp())
        file_mtime = int(os.path.getmtime(os.path.join(path, TOKEN_FILE)))
        if current_time - file_mtime < 84000:
            return token

    for course in canvas.get_courses():
        # Create a due date sorted list of assignments where the submission type 
        # is 'online_upload'. Basically we only want courses that can be
        # submitted via upload
        course_meta[course.id] = {
            "name": course.course_code,
            "assignments": sorted(({
                    "name": assignment.name,
                    "id": assignment.id,
                    "course": assignment.course_id,
                    "due_at": int(
                            datetime.strptime(
                                assignment.due_at,
                                UTC_FORMAT
                            ).replace(tzinfo=timezone.utc).timestamp()
                        ) if isinstance(assignment.due_at, str) else 0
                    
                } for assignment in course.get_assignments()
                    if "online_upload" in assignment.submission_types),
                key = lambda e: e["due_at"]
            )
        }
    
    token["courses"] = course_meta
    with open(os.path.join(path, TOKEN_FILE), "w") as fp:
        json.dump(token, fp, indent=4)
    return token


def canvas_create(path:str=None, tokenptr=[]) -> Canvas:
    """Create a Canvas object"""
    path = os.getcwd() if path is None else path
    with open(os.path.join(path, TOKEN_FILE), "r") as fp:
        token = json.load(fp)
    tokenptr.append(token)
    return Canvas(token["url"], token["token"])

if __name__ == "__main__":
    import argparse
    import logging

    logger = logging.getLogger()
    fmt = logging.Formatter(
        "%(asctime)s %(pathname)s:%(lineno)d - %(message)s"
    )
    hdlr = logging.StreamHandler()
    hdlr.setFormatter(fmt)
    logger.addHandler(hdlr)

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "command",
        choices=[
            "init",
            "serve",
            "pull",
            "del",
        ],
    )
    parser.add_argument(
        "-l",
        "--log",
        help="logging level",
        choices=["DEBUG","INFO","WARNING","ERROR","CRITICAL"],
        default="WARNING",
    )
    
    optarg = parser.parse_args()
    logger.setLevel(getattr(logging, optarg.log))
    
    if optarg.command == "init":
        dropbox_init()
    elif optarg.command == "serve":
        try:
            from .service import serve
        except:
            from service import serve
        serve()
    elif optarg.command == "pull":
        dropbox_pull(check_mtime=False)
