# canvas-dropbox

Upload assignments by copying it to folder.

Initialize dropbox which creates a `token.json` file
```
cd /path/to/dropbox
dropbox init
```
Then go to canvas and create a new access token and paste it into the 'token' field of `token.json`
```
dropbox pull
```
This obtains course and assignment data from your canvas account. This may take a few moments.
```
dropbox serve
```
Start the dropbox service. It'll create folders for all your classes which has assignments that requires a file upload if they don't exist. Dropping a file into that folder will create a window to select the exact assignment you wish to upload to.
